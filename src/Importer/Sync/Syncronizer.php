<?php

namespace Nodopiano\Feratel\Importer\Sync;

abstract class Syncronizer
{
    static $post_type = 'post';

    public function __construct($client)
    {
        $this->client = $client;
        $this->boot();
    }

    abstract protected function boot();
    public static function connect($post_ids)
    {
        if ($post_ids) {
            die(get_post_type($post_ids['it']));
            $post_type = get_post_type($post_ids['it']);
            // https://wpml.org/wpml-hook/wpml_element_type/
            $wpml_element_type = apply_filters('wpml_element_type', $post_type);
            // get the language info of the original post
            // https://wpml.org/wpml-hook/wpml_element_language_details/
            $get_language_args = array('element_id' => $post_ids['it'], 'element_type' => $post_type);
            $original_post_language_info = $get_language_args ? apply_filters('wpml_element_language_details', null, $get_language_args) : null;

            $set_language_args = $original_post_language_info ? array(
                'element_id'    => $post_ids['en'],
                'element_type'  => $wpml_element_type,
                'trid'   => $original_post_language_info->trid,
                'language_code'   => 'en',
                'source_language_code' => 'it'
            ) : null;


            if ($set_language_args) {
                do_action('wpml_set_element_language_details', $set_language_args);
            }

            $set_language_args = $original_post_language_info ? array(
                'element_id'    => $post_ids['de'],
                'element_type'  => $wpml_element_type,
                'trid'   => $original_post_language_info->trid,
                'language_code'   => 'de',
                'source_language_code' => 'it'
            ) : null;


            if ($set_language_args) {
                do_action('wpml_set_element_language_details', $set_language_args);
            }
        }
    }

    public function savePosts()
    {
        return collect($this->items)->map(function ($item) {
            $post_id = $this->saveMainLanguage($item);
            $langs = $this->saveLanguages($item, $post_id);
            return $langs;
        });
    }

    public function saveMainLanguage($item)
    {
        return $this->savePost($item['it'], 'it');
    }

    public function saveLanguages($item, $post_id)
    {
        return collect($item)->forget('it')->map(function ($post, $language) use ($post_id) {
            return $this->connectPost($post, $language, $post_id);
        });
    }
    public function connectPost($post, $language, $post_id)
    {
        try {
            $language_post_id = $this->savePost($post, $language);
            $this->connectLanguagePost($language_post_id, $post_id, $language);
        } catch (\Throwable $th) {
            \WP_CLI::line('cancello...');
            wp_delete_post($language_post_id);
        }
    }

    public function connectLanguagePost($language_post_id, $parent, $language)
    {
        $post_type = get_post_type($parent);
        // https://wpml.org/wpml-hook/wpml_element_type/
        $wpml_element_type = apply_filters('wpml_element_type', $post_type);
        // get the language info of the original post
        // https://wpml.org/wpml-hook/wpml_element_language_details/
        $get_language_args = array('element_id' => $parent, 'element_type' => $post_type);
        $original_post_language_info = $get_language_args ? apply_filters('wpml_element_language_details', null, $get_language_args) : null;

        $set_language_args = $original_post_language_info ? array(
            'element_id'    => $language_post_id,
            'element_type'  => $wpml_element_type,
            'trid'   => $original_post_language_info->trid,
            'language_code'   => $language,
            'source_language_code' => 'it'
        ) : null;


        if ($set_language_args) {
            do_action('wpml_set_element_language_details', $set_language_args);
        }
    }


    public function get()
    {
        return $this->savePosts();
    }

    public static function sync($client)
    {
        try {
            //code...
            //throw $th;
            $items = (new static($client))->get();
            return true;
        } catch (\Throwable $th) {
            dd($th);
            return false;
        }
    }
}
