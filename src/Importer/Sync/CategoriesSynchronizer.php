<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\KeyValues\Categories;

class CategoriesSynchronizer extends Syncronizer
{

    protected function boot()
    {
        $categories = (new Categories($this->client))->query(__DIR__ . '/../Requests/categories.xml')->get();
        $this->items = Categories::format($categories);
    }

    public function savePost($item, $language)
    {
        // Check if taxonomy is active on DSI
        if ($item['active'] && $item['is_event']) {
            // Check if already exists
            $taxonomy_id = term_exists($item['name']);
            if (!$taxonomy_id) {
                $inserted_taxonomy_id = wp_insert_term($item['name'], 'category');
                // In case of its existence I update it.
                if (is_wp_error($inserted_taxonomy_id)) {
                    $inserted_taxonomy_id = $inserted_taxonomy_id->error_data['term_exists'] ?? false;
                    if ($inserted_taxonomy_id) {
                        $inserted_taxonomy_id = wp_update_term($inserted_taxonomy_id, 'category', ['name' => $item['name']]);
                        echo $inserted_taxonomy_id['term_id'] . " - updating: category - " . $item['name'] . " - language: " . $language . "\r\n";
                    } else {
                        echo $inserted_taxonomy_id['term_id'] . " - category - " . $item['name'] . " - language: " . $language . "was a DOUBLE. \r\n";
                    }
                } else {
                    echo $inserted_taxonomy_id['term_id'] . " - importing: category - " . $item['name'] . " - language: " . $language . "\r\n";
                }
                $taxonomy_id = $inserted_taxonomy_id['term_id'] ?? false;
            } else {
                wp_update_term($taxonomy_id, 'category', ['name' => $item['name']]);
                echo $taxonomy_id . " - updating: category - " . $item['name'] . " - language: " . $language . "\r\n";
            }
            update_field('tosc5_id', $item['tosc_item_id'], 'category_' . $taxonomy_id);
            return $taxonomy_id;
        }
    }

    public function getTaxonomy($item)
    {
        // Controllo solo le entries in italiano
        $found_taxonomy = get_terms([
            'taxonomy'      => 'category',
            'name'          => $item['name'],
            'hide_empty'    => false
            /* 'meta_key'      => 'tosc5_id',
            'meta_value'    => $item['tosc_item_id'] */
        ]);
        return ($found_taxonomy && is_array($found_taxonomy) && !empty($found_taxonomy)) ? $found_taxonomy[0]->term_id : false;
    }

    public function connectLanguagePost($language_post_id, $parent, $language)
    {
        $post_type = 'category';
        // https://wpml.org/wpml-hook/wpml_element_type/
        $wpml_element_type = apply_filters('wpml_element_type', $post_type);
        // get the language info of the original post
        // https://wpml.org/wpml-hook/wpml_element_language_details/
        $get_language_args = array('element_id' => $parent, 'element_type' => $post_type);
        $original_post_language_info = $get_language_args ? apply_filters('wpml_element_language_details', null, $get_language_args) : null;

        $set_language_args = $original_post_language_info ? array(
            'element_id'            => $language_post_id,
            'element_type'          => $wpml_element_type,
            'trid'                  => $original_post_language_info->trid,
            'language_code'         => $language,
            'source_language_code'  => 'it'
        ) : null;


        if ($set_language_args) {
            do_action('wpml_set_element_language_details', $set_language_args);
        }
    }
}
