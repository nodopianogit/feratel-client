<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\BasicData\AdditionalServices;

use WP_Query;

use DateTime;

class ExperiencesSynchronizer extends Syncronizer
{

    static $post_type = 'experiences';

    public function boot()
    {
        $experiences = (new AdditionalServices($this->client))->query(__DIR__ . '/../Requests/additional-services.xml')->get();
        $this->items = AdditionalServices::format($experiences);
    }

    public function getCategories($item, $language)
    {
        $categories_list = [];
        $tosc5_categories = get_field('tosc5_categories', 'option');
        if (is_array($tosc5_categories) && !empty($tosc5_categories) && is_array($item['holidayThemes']) && !empty($item['holidayThemes'])) {
            foreach ($tosc5_categories as $tocs5_categories_item) {
                $category_tosc5_ids = array_map(function ($item) {
                    return $item['tosc5_topic_id'];
                }, $tocs5_categories_item['tosc5_topic_id_list']);

                foreach ($item['holidayThemes'] as $category) {
                    $categories_list[] = in_array($category, $category_tosc5_ids) ?
                        apply_filters('wpml_object_id', $tocs5_categories_item['tosc5_category'], 'category', TRUE, $language) :
                        null;
                }
            }
        } else {
            echo 'No categories' . "\r\n";
        }
        return $categories_list;
    }

    public function getPost($tosc_id, $key)
    {
        $found_post = new WP_Query([
            'post_type' => static::$post_type,
            'posts_per_page' => 1,
            'suppress_filters' => false,
            'post_status' => ['publish', 'draft', 'private'],
            'meta_key' => $key . '_tosc5_id',
            'meta_value' => $tosc_id,
        ]);
        return $found_post->have_posts() ? $found_post->posts[0]->ID : false;
    }

    public function savePost($item, $language)
    {
        if ($item['name']) {
            $item['name'] = ucfirst(str_replace(',', ' -', $item['name']));
            $categories = $this->getCategories($item, $language);

            $post_id = $item['tosc_item_id'] ? $this->getPost($item['tosc_item_id'], 'experience') : false;
            $post_id = !$post_id ? post_exists($item['name'], null, null, static::$post_type) : $post_id;
            if ($language !== 'it') {
                $post_id = function_exists('icl_object_id') ? icl_object_id($post_id, static::$post_type, false, $language) : $post_id;
            }

            if ($post_id) {
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => $item['name'],
                    'post_content' => is_array($item['descriptions']) ? str_replace('Array', '', join("", array_unique($item['descriptions']))) : $item['descriptions'],
                    'post_author' => 2
                ]);
                echo "$post_id - updating: " . $item['name'] . " - language: $language\r\n";
            } else {
                $new_post = array(
                    'post_title' => $item['name'],
                    'post_content' => is_array($item['descriptions']) ? str_replace('Array', '', join("", array_unique($item['descriptions']))) : $item['descriptions'],
                    'post_status' => 'publish',
                    // 'post_category' => (isset($categories) && !empty($categories)) ? $categories : [],
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_author' => 2,
                    'post_type' => 'experiences',
                );
                $post_id = wp_insert_post($new_post);
                echo "$post_id - importing: " . $item['name'] . " - language: $language\r\n";
            }

            $args = array('element_id' => $post_id, 'element_type' => 'experiences');
            $post_language = apply_filters('wpml_element_language_details', null, $args);

            // wp_set_post_categories($post_id, $categories, true);

            if (isset($item['dates']['Date'])) {
                if (count($item['dates']['Date']) > 1) {
                    $last_item = count($item['dates']['Date']) - 1;
                    $from = isset($item['dates']['Date'][0]['@attributes']['From']) ? $item['dates']['Date'][0]['@attributes']['From'] : '';
                    $to = isset($item['dates']['Date'][$last_item]['@attributes']['To']) ? $item['dates']['Date'][$last_item]['@attributes']['To'] : '';
                } else {
                    $from = (isset($item['dates']['Date']['@attributes']['From'])) ? $item['dates']['Date']['@attributes']['From'] : '';
                    $to = (isset($item['dates']['Date']['@attributes']['To'])) ? $item['dates']['Date']['@attributes']['To'] : '';
                }
            }

            if (isset($post_language->language_code) && ($post_language->language_code == $language || $post_language->language_code == '')) {
                $address = [];
                if (is_array($item['addresses']) && !empty($item['addresses'])) {
                    foreach ($item['addresses'] as $address_obj) {
                        if (isset($address_obj['AddressLine1']) && $address_obj['AddressLine1']) {
                            $zip_code = $address_obj['ZipCode'] ?? '';
                            $town = $address_obj['Town'] ?? '';
                            array_push($address, $address_obj['AddressLine1'] . ' - ' . $zip_code . ' ' . $town);
                        }
                    }
                }
                $address_string = (is_array($address) && !empty($address)) ? implode(' ', array_unique($address)) : '';
                $tosc_item_id = $item['tosc_item_id'] ?? null;
                $price = $item['price']['value'] ?? 0;
                $purchase_link = null; //$item['buy_link'] ?? null;

                $booking_contacts = null;
                if (!empty($item['contacts'])) {
                    foreach ($item['contacts'] as $contact) {
                        if (isset($contact['@attributes']['Type'])) {
                            $booking_address = (isset($contact['AddressLine1']) && !empty($contact['AddressLine1'])) ? $contact['AddressLine1'] : '';
                            $booking_address = (isset($contact['ZipCode'])) ? (($booking_address) ? $booking_address . ' - ' . $contact['ZipCode'] : $contact['ZipCode']) : '';
                            $booking_address = (isset($contact['Town'])) ? (($booking_address) ? $booking_address . ' - ' . $contact['Town'] : $contact['Town']) : '';
                            if ($contact['@attributes']['Type'] === 'Object' || $contact['@attributes']['Type'] === 'Owner') {
                                $booking_contacts['name'] = $contact['Company'] ?? null;
                                $booking_contacts['place'] = str_replace('Array', '', $booking_address);
                                $booking_contacts['tel'] = (isset($contact['Phone']) && !empty($contact['Phone'])) ? str_replace('+39+39', '+39', str_replace(' ', '', str_replace('(', '', str_replace('0039', '+39', str_replace(')', '', $contact['Phone']))))) : null;
                                $booking_contacts['email'] = (isset($contact['Email']) && !empty($contact['Email'])) ? $contact['Email'] : null;
                                $booking_contacts['url'] = (isset($contact['URL']) && !empty($contact['URL'])) ? (substr($contact['URL'], -1) === '/') ? substr($contact['URL'], 0, -1) : $contact['URL'] : null;
                            }
                        }
                    }
                }

                $to = ($from !== $to) ? $to : null;
                $is_for_family = in_array('090a18ae-5d50-421e-a65a-c5a2b8227ae5', $item['holidayThemes']);

                update_field('experience_tosc5_id', $tosc_item_id, $post_id);
                update_field('experience_place', $address_string, $post_id);
                update_field('experience_date', $from, $post_id);
                update_field('experience_date_end', $to, $post_id);
                update_field('experience_featured_image', $item['thumbnail'], $post_id);
                update_field('experience_family', $is_for_family, $post_id);
                update_field('experience_price', $price, $post_id);
                if (!empty($item['gallery'])) {
                    foreach ($item['gallery'] as $image) {
                        update_field('field_611ccc5b5665f', $item['gallery'], $post_id);
                    }
                }
                if (!empty($item['attachments'])) {
                    update_field('field_61926905f742c', $item['attachments'], $post_id);
                }
                //update_field('experience_purchase_link', $purchase_link, $post_id);
                if ($booking_contacts) {
                    foreach ($booking_contacts as $field => $value) {
                        update_field("experience_booking_contacts_$field", $value, $post_id);
                    }
                }
            }
            return $post_id;
        }
        return false;
    }
}
