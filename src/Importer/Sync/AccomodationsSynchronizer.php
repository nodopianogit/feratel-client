<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\BasicData\Accomodations;
use Nodopiano\Feratel\BasicData\Packages;

use WP_Query;

class AccomodationsSynchronizer extends Syncronizer
{
    static $post_type = 'hospitality';

    public function boot()
    {
        $accomodations = (new Accomodations($this->client))->query(__DIR__ . '/../Requests/serviceprovider.xml')->get();
        $this->items = Accomodations::format($accomodations);
    }

    public function getPost($tosc_id, $key)
    {
        $found_post = new WP_Query([
            'post_type' => static::$post_type,
            'posts_per_page' => 1,
            'suppress_filters' => false,
            'post_status' => ['publish', 'draft', 'private'],
            'meta_key' => $key . '_tosc5_id',
            'meta_value' => $tosc_id,
        ]);
        return $found_post->have_posts() ? $found_post->posts[0]->ID : false;
    }

    public function savePost($item, $language)
    {
        $post_id = $item['tosc_item_id'] ? $this->getPost($item['tosc_item_id'], 'hospitality') : false;
        $post_id = !$post_id ? post_exists(ucfirst(strtolower($item['name'])), null, null, 'hospitality') : $post_id;
        $default_cat = get_category_by_slug('alloggi') ? get_category_by_slug('alloggi')->term_id : null;
        $post_content = is_array($item['descriptions']) ? join("", array_unique($item['descriptions'])) : $item['descriptions'];

        if ($language !== 'it') {
            $post_id = function_exists('icl_object_id') ? icl_object_id($post_id, 'hospitality', false, $language) : $post_id;
            $default_cat = $default_cat ? apply_filters('wpml_object_id', $default_cat, 'category', TRUE, $language) : $default_cat;
        }
        if (!$post_id) {
            $new_post = array(
                'post_title' => ucfirst(strtolower($item['name'])),
                'post_content' => is_array($item['descriptions']) ? str_replace('Array', '', join("", array_unique($item['descriptions']))) : $item['descriptions'],
                'post_status' => 'publish',
                'post_category' => [$default_cat],
                'post_date' => date('Y-m-d H:i:s'),
                'post_author' => 2,
                'post_type' => 'hospitality',
            );
            $post_id = wp_insert_post($new_post);
            echo "$post_id - importing: " . $item['name'] . " - language: $language\r\n";
        } else {
            wp_update_post([
                'ID' => $post_id,
                'post_title' => ucfirst(strtolower($item['name'])),
                'post_content' =>  str_replace('Array', '', $post_content),
                'post_author' => 2
            ]);
            echo "$post_id - updating: " . $item['name'] . " - language: $language\r\n";
        }

        $args = array('element_id' => $post_id, 'element_type' => 'events');
        $post_language = apply_filters('wpml_element_language_details', null, $args);
        $address_string = '';

        if (isset($post_language->language_code) && ($post_language->language_code == $language || $post_language->language_code == '')) {
            if (isset($item['position']['Longitude'])) {
                if (is_array($item['addresses']) && !empty($item['addresses'])) {
                    foreach ($item['addresses'] as $address) {
                        if (isset($address['@attributes']['Type']) && $address['@attributes']['Type'] === 'Object') {
                            $address_string = (isset($address['AddressLine1']) && !empty($address['AddressLine1'])) ? $address['AddressLine1'] : '';
                            $address_string .= (isset($address['ZipCode']) && !empty($address['ZipCode'])) ? ($address_string) ? ' - ' . $address['ZipCode'] : $address['ZipCode'] : '';
                            $address_string .= (isset($address['Town']) && !empty($address['Town'])) ? ($address_string) ? ' - ' . $address['Town'] : $address['Town'] : '';
                        }
                    }
                }

                $latitude = $item['position']['Latitude'] ?? null;
                $location = ['address' => $address_string, 'zoom' => 14, 'lng' => $item['position']['Longitude'], 'lat' => $latitude];
                update_field('hospitality_place', $location, $post_id);
            }
            $url = (isset($item['addresses'][0]['URL']) && !is_array($item['addresses'][0]['URL'])) ? ['url' => strtolower($item['addresses'][0]['URL']), 'target' => '_blank'] : '';
            $emails = $this->formatRepeater($item['addresses'], 'hospitality_email', 'Email');
            $phones = $this->formatRepeater($item['addresses'], 'hospitality_phone', 'Phone');
            $tosc_item_id = $item['tosc_item_id'] ?? null;

            wp_set_post_categories($post_id, [$default_cat], true);

            update_field('hospitality_tosc5_id', $tosc_item_id, $post_id);
            update_field('hospitality_featured_image', $item['thumbnail'], $post_id);
            update_field('hospitality_email_addresses', $emails, $post_id);
            update_field('hospitality_phone_numbers', $phones, $post_id);
            update_field('hospitality_url', $url, $post_id);
            //update_field('hospitality_gallery', $item['gallery'], $post_id);
        }
        return $post_id;
    }

    public function formatRepeater($items, $key, $value)
    {
        $repeater = collect($items)->map(function ($item) use ($key, $value) {
            if (isset($item[$value])) {
                return [
                    $key => str_replace('0039', '+39', str_replace(' ', '', str_replace('(', '', str_replace(')', '', $item[$value]))))
                ];
            }
        })->toArray();
        return array_unique($repeater, SORT_REGULAR);
    }
}
