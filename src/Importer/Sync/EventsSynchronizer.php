<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\BasicData\Events;

use WP_Query;

use DateTime;

class EventsSynchronizer extends Syncronizer
{
    static $post_type = 'events';

    protected function boot()
    {
        $events = (new Events($this->client))->query(__DIR__ . '/../Requests/events.xml')->get();
        $this->items = Events::format($events);
    }

    public function getCategories($item, $language)
    {
        $categories_list = [];
        $tosc5_categories = get_field('tosc5_categories', 'option');
        if (is_array($tosc5_categories) && !empty($tosc5_categories) && is_array($item['holidayThemes']) && !empty($item['holidayThemes'])) {
            foreach ($tosc5_categories as $tocs5_categories_item) {
                $category_tosc5_ids = array_map(function ($item) {
                    return $item['tosc5_topic_id'];
                }, $tocs5_categories_item['tosc5_topic_id_list']);

                foreach ($item['holidayThemes'] as $category) {
                    $categories_list[] = in_array($category, $category_tosc5_ids) ?
                        apply_filters('wpml_object_id', $tocs5_categories_item['tosc5_category'], 'category', TRUE, $language) :
                        null;
                }
            }
        } else {
            echo 'No categories' . "\r\n";
        }
        return $categories_list;
    }

    public function getPost($tosc_id, $key)
    {
        $found_post = new WP_Query([
            'post_type' => static::$post_type,
            'posts_per_page' => 1,
            'suppress_filters' => false,
            'post_status' => ['publish', 'draft', 'private'],
            'meta_key' => $key . '_tosc5_id',
            'meta_value' => $tosc_id,
        ]);
        return $found_post->have_posts() ? $found_post->posts[0]->ID : false;
    }

    public function savePost($item, $language)
    {
        $categories = $this->getCategories($item, $language);

        $post_id = $item['tosc_item_id'] ? $this->getPost($item['tosc_item_id'], 'event') : false;
        //$post_id = !$post_id ? post_exists($item['name'], null, null, static::$post_type) : $post_id;
        if ($language !== 'it') {
            $post_id = function_exists('icl_object_id') ? icl_object_id($post_id, static::$post_type, false, $language) : $post_id;
        }
        $is_test = $item['name'] === 'test' || $item['name'] === 'Test' || $item['name'] === 'TEST' || stripos($item['name'], 'test') !== false ? true : false;
        if (!$is_test) {
            if (!$post_id) {
                $new_post = array(
                    'post_title' => $item['name'],
                    'post_content' => is_array($item['descriptions']) ? str_replace('Array', '', join("", array_unique($item['descriptions']))) : $item['descriptions'],
                    'post_status' => 'publish',
                    'post_category' => (isset($categories) && !empty($categories)) ? $categories : [],
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_author' => 2,
                    'post_type' => 'events',
                );
                $post_id = wp_insert_post($new_post);
                echo "$post_id - importing: " . $item['name'] . " - language: $language\r\n";
            } else {
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => $item['name'],
                    'post_author' => 2
                ]);
                echo "$post_id - updating: " . $item['name'] . " - language: $language\r\n";
            }
        }

        $args = array('element_id' => $post_id, 'element_type' => 'events');
        $post_language = apply_filters('wpml_element_language_details', null, $args);

        wp_set_post_categories($post_id, $categories, true);

        if (isset($item['dates']['Date'])) {
            if (count($item['dates']['Date']) > 1) {
                $last_item = count($item['dates']['Date']) - 1;
                $from = isset($item['dates']['Date'][0]['@attributes']['From']) ? $item['dates']['Date'][0]['@attributes']['From'] : '';
                $to = isset($item['dates']['Date'][$last_item]['@attributes']['To']) ? $item['dates']['Date'][$last_item]['@attributes']['To'] : '';
            } else {
                $from = (isset($item['dates']['Date']['@attributes']['From'])) ? $item['dates']['Date']['@attributes']['From'] : '';
                $to = (isset($item['dates']['Date']['@attributes']['To'])) ? $item['dates']['Date']['@attributes']['To'] : '';
            }
            $to = ($from !== $to) ? $to : null;
            update_field('event_date', $from, $post_id);
            update_field('event_date_end', $to, $post_id);
        }

        if (isset($post_language->language_code) && ($post_language->language_code == $language || $post_language->language_code == '')) {
            $start_time = $item['time']['StartTime']['@attributes']['Time'] ?? '';
            $venue = '';
            $organizer = null;
            $booking_contacts = null;
            $tosc_item_id = $item['tosc_item_id'] ?? null;
            if (!empty($item['contacts'])) {
                foreach ($item['contacts'] as $contact) {
                    if (isset($contact['@attributes']['Type'])) {
                        $venue = !empty($item['location']) ? str_replace(',', ', ', str_replace(',  ', ', ', $item['location'])) : '';
                        $address = (isset($contact['AddressLine1']) && !empty($contact['AddressLine1'])) ? $contact['AddressLine1'] : '';
                        $address .= (isset($contact['ZipCode']) && !empty($contact['ZipCode'])) ? ($address) ? ' - ' . $contact['ZipCode'] : $contact['ZipCode'] : '';
                        $address .= (isset($contact['Town']) && !empty($contact['Town'])) ? ($address) ? ' - ' . $contact['Town'] : $contact['Town'] : '';

                        if ($contact['@attributes']['Type'] === 'Venue') {
                            $venue = (isset($contact['AddressLine1']) && !empty($contact['AddressLine1'])) ? $contact['AddressLine1'] : $venue;
                            $venue .= (isset($contact['ZipCode']) && !empty($contact['ZipCode'])) ? ($venue) ? ' - ' . $contact['ZipCode'] : $contact['ZipCode'] : '';
                            $venue .= (isset($contact['Town']) && !empty($contact['Town'])) ? ($venue) ? ' - ' . $contact['Town'] : $contact['Town'] : '';
                        }
                        if ($contact['@attributes']['Type'] === 'Organizer') {
                            $organizer['name'] = $contact['Company'] ?? null;
                            $organizer['place'] = str_replace('Array', '', $address);
                            $organizer['tel'] = (isset($contact['Phone']) && !empty($contact['Phone'])) ? str_replace('+39+39', '+39', str_replace(' ', '', str_replace('(', '', str_replace('0039', '+39', str_replace(')', '', $contact['Phone']))))) : null;
                            $organizer['email'] = (isset($contact['Email']) && !empty($contact['Email'])) ? $contact['Email'] : null;
                            $organizer['url'] = (isset($contact['URL']) && !empty($contact['URL'])) ? (substr($contact['URL'], -1) === '/') ? substr($contact['URL'], 0, -1) : $contact['URL'] : null;
                        }
                        if ($contact['@attributes']['Type'] === 'Booking') {
                            $booking_contacts['name'] = $contact['Company'] ?? null;
                            $booking_contacts['place'] = str_replace('Array', '', $address);
                            $booking_contacts['tel'] = (isset($contact['Phone']) && !empty($contact['Phone'])) ? str_replace('+39+39', '+39', str_replace(' ', '', str_replace('(', '', str_replace('0039', '+39', str_replace(')', '', $contact['Phone']))))) : null;
                            $booking_contacts['email'] = (isset($contact['Email']) && !empty($contact['Email'])) ? $contact['Email'] : null;
                            $booking_contacts['url'] = (isset($contact['URL']) && !empty($contact['URL'])) ? (substr($contact['URL'], -1) === '/') ? substr($contact['URL'], 0, -1) : $contact['URL'] : null;
                        }
                    }
                }
            }

            $location = ['address' => $venue, 'zoom' => 14, 'lng' => $item['position']['Longitude'], 'lat' => $item['position']['Latitude']];

            /* if ($item['tosc_item_id'] === '10d5f7c3-b8c9-4386-8c60-72d60d1c5f8c') {
                var_dump('gallery', $item['gallery']);
                var_dump('attachments', $item['attachments']);
            } */

            update_field('event_tosc5_id', $tosc_item_id, $post_id);
            update_field('event_start_time', $start_time, $post_id);
            update_field('event_place', $location, $post_id);
            update_field('event_featured_image', $item['thumbnail'], $post_id);
            if ($organizer) {
                foreach ($organizer as $field => $value) {
                    update_field("event_organizer_$field", $value, $post_id);
                }
            }
            if (!empty($item['gallery'])) {
                update_field('field_611ccba0a0800', $item['gallery'], $post_id);
            }
            if (!empty($item['attachments'])) {
                update_field('field_61795c30af71d', $item['attachments'], $post_id);
            }
            if ($booking_contacts) {
                foreach ($booking_contacts as $field => $value) {
                    update_field("event_booking_contacts_$field", $value, $post_id);
                }
            }
        }
        return $post_id;
    }
}
