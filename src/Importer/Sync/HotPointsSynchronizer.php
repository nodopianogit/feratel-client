<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\BasicData\HotPoints;
use WP_Query;

class HotPointsSynchronizer extends Syncronizer
{

    static $post_type = 'hot-points';

    static $post_types = ['hot-points', 'itineraries', 'hospitality'];

    public function boot()
    {
        $hot_points = (new HotPoints($this->client))->query(__DIR__ . '/../Requests/hot-points.xml')->get();
        $this->items = HotPoints::format($hot_points);
    }

    public function catalogPost($item, $language)
    {
        $post_types_match = [
            'Type-1' => ['post_type' => 'hospitality', 'category' => apply_filters('wpml_object_id', 37, 'category', TRUE, $language)], // Ristori
            'Type-2' => ['post_type' => 'itineraries', 'category' => []],
            'Type-3' => ['post_type' => 'hospitality', 'category' => apply_filters('wpml_object_id', 38, 'category', TRUE, $language)], // Servizi
            'Type-4' => ['post_type' => 'hospitality', 'category' => apply_filters('wpml_object_id', 38, 'category', TRUE, $language)], // Servizi
            'Type-5' => ['post_type' => 'hot-points', 'category' => apply_filters('wpml_object_id', 232, 'category', TRUE, $language)], // Musei
            'Type-6' => ['post_type' => 'hot-points', 'category' => []],
            'Type-7' => ['post_type' => 'hospitality', 'category' => apply_filters('wpml_object_id', 38, 'category', TRUE, $language)], // Servizi
            'Type-8' => ['post_type' => 'hospitality', 'category' => apply_filters('wpml_object_id', 38, 'category', TRUE, $language)] // Servizi
        ];
        $post_type = 'hot-points';
        $categories = [];
        $title = strtolower($item['name']);
        $tosc5_categories = get_field('tosc5_categories', 'option');

        if (is_array($tosc5_categories) && !empty($tosc5_categories) && is_array($item['topics']) && !empty($item['topics'])) {
            foreach ($tosc5_categories as $tocs5_categories_item) {
                $category_tosc5_ids = array_map(function ($item) {
                    return $item['tosc5_topic_id'];
                }, $tocs5_categories_item['tosc5_topic_id_list']);

                foreach ($item['topics'] as $type => $topics) {
                    $post_type = $post_types_match['Type-' . strval($type)]['post_type'] ?? $post_type;

                    if (is_array($topics) && !empty($topics)) {
                        foreach ($topics as $topic) {
                            $categories[] = in_array($topic, $category_tosc5_ids) ? apply_filters('wpml_object_id', $tocs5_categories_item['tosc5_category'], 'category', TRUE, $language) : $post_types_match['Type-' . $type]['category'];
                        }
                    }

                    // Guide turistiche - Persone
                    if (strval($type) === '2') {
                        if (str_contains($title, 'guida') || str_contains($title, 'guide') || str_contains($title, 'accompagnatore') || str_contains($title, 'accompagnatrice')) {
                            $post_type = 'hospitality';
                        }
                    }
                }
            }
        } else {
            echo 'No topics' . "\r\n";
        }
        return ['post_type' => $post_type, 'categories' => $categories];
    }

    public function getPost($tosc_id, $post_type = 'hot-points', $key)
    {
        $found_post = new WP_Query([
            'post_type' => $post_type,
            'posts_per_page' => 1,
            'suppress_filters' => false,
            'post_status' => ['publish', 'draft', 'private'],
            'meta_key' => $key . '_tosc5_id',
            'meta_value' => $tosc_id,
        ]);
        return $found_post->have_posts() ? $found_post->posts[0]->ID : false;
    }

    public function generateContent($post_content)
    {
        $content = $post_content['long'] ?? '';
        $content .= ($post_content['prices'] && isset($post_content['prices'])) ? '<br/><h3>' . __('Informazioni sul prezzo', 'dprealpi') . '</h3>' . $post_content['prices'] : '';
        $content .= ($post_content['hints'] && isset($post_content['hints'])) ? '<br/><h3>' . __('Informazioni utili', 'dprealpi') . '</h3>' . $post_content['hints'] : '';
        return $content;
    }

    public function savePost($item, $language)
    {
        if (isset($item['name']) && $item['name']) {
            $post_type = $this->catalogPost($item, $language)['post_type'];
            $item['categories'] = $this->catalogPost($item, $language)['categories'];

            foreach (static::$post_types as $type) {
                $key = 'hot_point';
                $key = $type === 'itineraries' ? 'itinerary' : $key;
                $key = $type === 'hospitality' ? 'hospitality' : $key;
                $post_id = $item['tosc_item_id'] ? $this->getPost($item['tosc_item_id'], $type, $key) : false;
            }

            if (!$post_id) {
                foreach (static::$post_types as $type) {
                    $post_id = !$post_id ? post_exists($item['name'], null, null, $type) : $post_id;
                }
            }

            $found_type = get_post_type($post_id);
            if ($language !== 'it') {
                $post_id = function_exists('icl_object_id') ? icl_object_id($post_id, $found_type, false, $language) : false;
            }

            if (!$post_id) {
                $new_post = array(
                    'post_title' => $item['name'],
                    'post_content' => is_array($item['descriptions']) ? $this->generateContent($item['descriptions']) : $item['descriptions'],
                    'post_excerpt' => $item['excerpt'] ?? '',
                    'post_status' => 'publish',
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_category' => (isset($item['categories']) && !empty($item['categories'])) ? [$item['categories']] : [],
                    'post_author' => 2,
                    'post_type' => $post_type,
                );
                $post_id = wp_insert_post($new_post);
                echo "$post_id - importing: " . $post_type . ' - ' . $item['name'] . " - language: $language\r\n";
            } else {
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => $item['name'],
                    'post_type' => $post_type,
                    'post_content' => is_array($item['descriptions']) ? $this->generateContent($item['descriptions']) : $item['descriptions'],
                    'post_excerpt' => $item['excerpt'] ?? '',
                    'post_author' => 2
                ]);
                echo "$post_id - updating: " . $post_type . ' - ' . $item['name'] . " - " . $item['thumbnail'] . " - language: $language\r\n";
            }

            $key = 'hot_point';
            $gallery_field_key = 'field_60e786eec1037';
            if ($post_type === 'itineraries') {
                $key = 'itinerary';
                $gallery_field_key = 'field_60e71db74628a';
                $attachment_field_key = 'field_6192632821f15';
            } elseif ($post_type === 'hospitality') {
                $key = 'hospitality';
                $gallery_field_key = 'field_60e786d2c1034';
                $attachment_field_key = 'field_61797033ccc3e';
            }

            $args = array('element_id' => $post_id, 'element_type' => $post_type);
            $post_language = apply_filters('wpml_element_language_details', null, $args);
            $address_string = '';

            if (isset($post_language->language_code) && ($post_language->language_code == $language || $post_language->language_code == '')) {
                if (is_array($item['addresses']) && !empty($item['addresses'])) {
                    foreach ($item['addresses'] as $address) {
                        if (isset($address['@attributes']['Type']) && $address['@attributes']['Type'] === 'InfrastructureExternal') {
                            $address_string = (isset($address['AddressLine1']) && !empty($address['AddressLine1'])) ? $address['AddressLine1'] : '';
                            $address_string .= (isset($address['ZipCode']) && !empty($address['ZipCode'])) ? ($address_string) ? ' - ' . $address['ZipCode'] : $address['ZipCode'] : '';
                            $address_string .= (isset($address['Town']) && !empty($address['Town'])) ? ($address_string) ? ' - ' . $address['Town'] : $address['Town'] : '';
                        }
                    }
                }
                $location = ['address' => $address_string, 'zoom' => 14, 'lng' => $item['position']['Longitude'], 'lat' => $item['position']['Latitude']];

                if ($key === 'hospitality') {
                    $url = (isset($item['addresses'][0]['URL']) && !is_array($item['addresses'][0]['URL'])) ? ['url' => strtolower($item['addresses'][0]['URL']), 'target' => '_blank'] : '';
                    $emails = $this->formatRepeater($item['addresses'], 'hospitality_email', 'Email');
                    $phones = $this->formatRepeater($item['addresses'], 'hospitality_phone', 'Phone');
                    $tosc_item_id = $item['tosc_item_id'] ?? null;

                    update_field('hospitality_url', $url, $post_id);
                    update_field('hospitality_email_addresses', $emails, $post_id);
                    update_field('hospitality_phone_numbers', $phones, $post_id);
                    update_field('hospitality_tosc5_id', $tosc_item_id, $post_id);
                }

                wp_set_post_categories($post_id, $item['categories'], true);

                $booking_contacts = null;
                if (is_array($item['addresses']) && !empty($item['addresses'])) {
                    foreach ($item['addresses'] as $contact) {
                        $contact = isset($contact['@attributes']['Type']) ? $contact : $item['addresses'];
                        $booking_contacts['name'] = $contact['Company'] ?? null;
                        $booking_contacts['tel'] = (isset($contact['Phone']) && !empty($contact['Phone'])) ? str_replace('+39+39', '+39', str_replace(' ', '', str_replace('(', '', str_replace('0039', '+39', str_replace(')', '', $contact['Phone']))))) : null;
                        $booking_contacts['email'] = (isset($contact['Email']) && !empty($contact['Email'])) ? $contact['Email'] : null;
                        $booking_contacts['url'] = (isset($contact['URL']) && !empty($contact['URL'])) ? (substr($contact['URL'], -1) === '/') ? substr($contact['URL'], 0, -1) : $contact['URL'] : null;
                    }
                }
                if ($item['tosc_item_id']) {
                    update_field($key . '_tosc5_id', $item['tosc_item_id'], $post_id);
                }
                update_field($key . '_featured_image', $item['thumbnail'], $post_id);
                update_field($key . '_place', $location, $post_id);

                /* if ($item['tosc_item_id'] === 'f6f43f20-a7a9-4386-bd9d-0a120fc81046') {
                    var_dump($item['descriptions']);
                    die();
                } */

                if (!empty($item['gallery'])) {
                    update_field($gallery_field_key, $item['gallery'], $post_id);
                }
                if (!empty($item['attachments'])) {
                    update_field($attachment_field_key, $item['attachments'], $post_id);
                }
                if ($booking_contacts) {
                    foreach ($booking_contacts as $field => $value) {
                        update_field($key . '_booking_contacts_' . $field, $value, $post_id);
                    }
                }
            }
            return $post_id;
        }
        return false;
    }

    public function formatRepeater($items, $key, $value)
    {
        $repeater = collect($items)->map(function ($item) use ($key, $value) {
            if (isset($item[$value])) {
                return [
                    $key => str_replace('0039', '+39', str_replace(' ', '', str_replace('(', '', str_replace(')', '', $item[$value]))))
                ];
            }
        })->toArray();
        return array_unique($repeater, SORT_REGULAR);
    }
}
