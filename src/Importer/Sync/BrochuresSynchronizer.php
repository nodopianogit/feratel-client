<?php

namespace Nodopiano\Feratel\Importer\Sync;

use Nodopiano\Feratel\BasicData\Brochures;

use WP_Query;

class BrochuresSynchronizer extends Syncronizer
{

    static $post_type = 'brochures';

    public function boot()
    {
        $brochures = (new Brochures($this->client))->query(__DIR__ . '/../Requests/brochures.xml')->get();
        $this->items = Brochures::format($brochures);
    }

    public function getPost($tosc_id, $key)
    {
        $found_post = new WP_Query([
            'post_type' => static::$post_type,
            'posts_per_page' => 1,
            'suppress_filters' => false,
            'post_status' => ['publish', 'draft', 'private'],
            'meta_key' => $key . '_tosc5_id',
            'meta_value' => $tosc_id
        ]);
        return $found_post->have_posts() ? $found_post->posts[0]->ID : false;
    }

    public function savePost($item, $language)
    {
        if (isset($item['name']) && $item['name']) {
            $post_id = $item['tosc_item_id'] ? $this->getPost($item['tosc_item_id'], 'brochure') : false;
            $post_id = !$post_id ? post_exists($item['name'], null, null, static::$post_type) : $post_id;

            if (!$post_id) {
                $new_post = array(
                    'post_title' => $item['name'],
                    'post_status' => is_array($item['documents']) && !empty($item['documents']) ? 'publish' : 'draft',
                    'post_date' => date('Y-m-d H:i:s'),
                    'post_author' => 2,
                    'post_type' => 'brochures',
                );
                $post_id = wp_insert_post($new_post);
                echo "$post_id - importing: " . $item['name'] . " - language: $language\r\n";
            } else {
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => $item['name'],
                    'post_author' => 2
                ]);
                echo "$post_id - updating: " . $item['name'] . " - language: $language\r\n";
            }
            $tosc_item_id = $item['tosc_item_id'] ?? null;
            $url = isset($item['url']) ? $item['url'] : '#';
            echo "$post_id - updating: " . $item['name'] . " - language: $language\r\n";

            $thumbnails = '';
            if (is_array($item['thumbnails']) && !empty($item['thumbnails'])) {
                if (isset($item['thumbnails'][0]['URL'])) {
                    update_field('brochure_thumbnail', $item['thumbnails'][0]['URL'], $post_id);
                }
            }

            update_field('brochure_tosc5_id', $tosc_item_id, $post_id);

            $documents = [];
            if (is_array($item['documents']) && !empty($item['documents'])) {
                foreach ($item['documents'] as $key => $doc) {
                    if (isset($doc['URL'])) {
                        $documents[$key] = ['brochure_url' => $doc['URL']];
                    }
                }
            }
            if (!empty($documents)) {
                update_field('field_60228fc9adf50', $documents, $post_id);
            }
            return $post_id;
        }
        return false;
    }
}
