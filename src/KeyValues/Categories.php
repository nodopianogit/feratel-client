<?php

namespace Nodopiano\Feratel\KeyValues;

use Tightenco\Collect\Support\Collection;

class Categories extends KeyValues
{
    public function get()
    {
        $categories = $this->results['Result']['KeyValues']['FacilityGroups']['FacilityGroup'];
        return Collection::make($categories);
    }

    public static function getItemDetails($item, $language)
    {
        $return = [
            'active' => $item['@attributes']['Active'] === 'true',
            'global' => $item['@attributes']['Global'] === 'true',
            'is_event' => $item['@attributes']['Type'] === 'Event',
            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => self::getAttribute('Name', $item, $language)
        ];
        return $return;
    }
}
