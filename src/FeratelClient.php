<?php

namespace Nodopiano\Feratel;

use Carbon\Carbon;
use Collator;
use SoapClient;
use Tightenco\Collect\Support\Collection;
use Mtownsend\XmlToArray\XmlToArray;

class FeratelClient
{
    protected $client;
    protected $company;
    protected $itemId;
    protected $options;

    public function __construct($company, $itemId, $originator = 'FERATEL', $url = 'http://interfacetest.deskline.net/DSI/BasicData.asmx?WSDL')
    {
        $this->company = $company;
        $this->itemId = $itemId;
        $this->originator = $originator;
        $this->client = new SoapClient($url . '?WSDL', [
            'trace' => true,
            'location' => $url,
            'uri' => $url,
        ]);
    }

    public function load($optionFile)
    {
        if (file_exists($optionFile)) {
            $this->options = simplexml_load_file($optionFile);
            $this->options->Request->Range->Item['Id'] = $this->itemId;
            $this->options->Request['Company'] = $this->company;
            $this->options->Request['Originator'] = $this->originator;
            if ($this->options->Request->BasicData->Filters && $this->options->Request->BasicData->Filters->Events) {
                $this->options->Request->BasicData->Filters->Events['Start'] = date('Y-m-d');
                $year = date('Y') + 1;
                $this->options->Request->BasicData->Filters->Events['End'] = date($year . '-m-d');
            }
        }
        return $this;
    }

    public function getData()
    {
        if ($this->options) {
            $results = $this->client->GetData(['xmlString' => $this->options->asXML()]);
            $convert = XmlToArray::convert($results->GetDataResult);
            return $convert;
        }
        return simplexml_load_string('');
    }

    public function getKeyValues()
    {
        if ($this->options) {
            $results = $this->client->GetKeyValues(['xmlString' => $this->options->asXML()]);
            $convert = XmlToArray::convert($results->GetKeyValuesResult);
            return $convert;
        }
        return simplexml_load_string('');
    }
}
