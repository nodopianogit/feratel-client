<?php

namespace Nodopiano\Feratel\BasicData;

use Tightenco\Collect\Support\Collection;

class Events extends BasicData
{
    public function get()
    {
        $events = $this->results['Result']['Events']['Event'];
        return Collection::make($events);
    }

    public static function getItemDetails($item, $language)
    {
        $gallery = self::getGallery($item, $language);
        $return = [
            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => self::getAttribute('Names', $item, $language),
            'location' => self::getAttribute('Location', $item, $language),
            'position' => self::getDetail($item, 'Position')['@attributes'] ?? '',
            'dates' => self::getDetail($item, 'Dates'),
            'time' => self::getDetail($item, 'StartTimes'),
            'descriptions' => self::getDescriptions($item, $language),
            'duration' => self::getDetail($item, 'Duration')['@content'],
            'thumbnail' => (!empty($gallery) && isset($gallery[0]['url'])) ? $gallery[0]['url'] : '',
            'gallery' => $gallery,
            'attachments' => self::getAttachments($item, $language),
            'contacts' => self::getAddresses($item, 'Address'),
            'holidayThemes' => static::getHolidayThemes($item) ?? null
        ];
        return $return;
    }

    public static function getDescriptions($item, $language)
    {
        $description = [];

        if (isset($item['Descriptions'])) {
            foreach ($item['Descriptions']['Description'] as $value) {
                if (isset($value['@attributes']) && isset($value['@attributes']['Language']) && $value['@attributes']['Language'] == $language) {
                    $description[] = $value['@content'] ?? '';
                }
            }
        }

        return $description;
    }

    public static function getThumbnail($item)
    {
        if (isset($item['Documents']['Document']['URL']) && (str_contains($item['Documents']['Document']['URL'], '.jpg') || str_contains($item['Documents']['Document']['URL'], '.jpeg') || str_contains($item['Documents']['Document']['URL'], '.png') || str_contains($item['Documents']['Document']['URL'], '.webp'))) {
            return $item['Documents']['Document']['URL'];
        } elseif (isset($item['Documents']['Document'][0]) && isset($item['Documents']['Document'][0]['URL']) && (str_contains($item['Documents']['Document'][0]['URL'], '.jpg') || str_contains($item['Documents']['Document'][0]['URL'], '.jpeg') || str_contains($item['Documents']['Document'][0]['URL'], '.png') || str_contains($item['Documents']['Document'][0]['URL'], '.webp'))) {
            return $item['Documents']['Document'][0]['URL'];
        }
    }

    public static function attachmentTitle($attachment, $language)
    {
        if (isset($attachment['Names']) && !empty($attachment['Names'])) {
            foreach ($attachment['Names'] as $translations) {
                if (is_array($translations) && !empty($translations)) {
                    foreach ($translations as $translation) {
                        if (isset($translation['@attributes']['Language']) && $translation['@attributes']['Language'] === $language) {
                            return $translation['@content'] ?? '';
                        }
                    }
                }
            }
        }
    }

    public static function isImage($image, $caption)
    {
        if (isset($image['URL']) && (str_contains($image['URL'], '.jpg') || str_contains($image['URL'], '.jpeg') || str_contains($image['URL'], '.png') || str_contains($image['URL'], '.webp'))) {
            return ['url' => $image['URL'], 'caption' => $caption];
        }
    }

    public static function isFile($file, $caption)
    {
        if (isset($file['URL']) && (!str_contains($file['URL'], '.jpg') && !str_contains($file['URL'], '.jpeg') && !str_contains($file['URL'], '.png') && !str_contains($file['URL'], '.webp'))) {
            return ['url' => $file['URL'], 'caption' => $caption];
        }
    }

    public static function getGallery($item, $language)
    {
        $gallery = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents']['Document'] as $image) {
                $caption = self::attachmentTitle($image, $language);
                if (self::isImage($image, $caption)) {
                    $gallery[] = self::isImage($image, $caption);
                }
            }
        }
        return $gallery;
    }

    public static function getAttachments($item, $language)
    {
        $attachments = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents']['Document'] as $file) {
                $caption = self::attachmentTitle($file, $language);
                if (self::isFile($file, $caption)) {
                    $attachments[] = self::isFile($file, $caption);
                }
            }
        }
        return $attachments;
    }
}
