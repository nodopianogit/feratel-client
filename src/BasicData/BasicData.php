<?php

namespace Nodopiano\Feratel\BasicData;

use Nodopiano\Feratel\FeratelClient;

abstract class BasicData
{

    protected $client;

    public function __construct(FeratelClient $client)
    {
        $this->client = $client;
    }

    public function query($requestFile)
    {
        $this->results = $this->client->load($requestFile)->getData();
        return $this;
    }

    public function get()
    {
        return collect([]);
    }

    public static function format($items)
    {
        return $items->map(function ($item) {
            return [
                'it' => static::getItemDetails($item, 'it'),
                'de' => static::getItemDetails($item, 'de'),
                'en' => static::getItemDetails($item, 'en'),
            ];
        });
    }

    public static function toJson(Collection $items)
    {
        return $items->toJson();
    }

    abstract public static function getItemDetails($item, $language);

    public static function getDetail($item, $detail)
    {
        return $item['Details'][$detail] ?? '';
    }

    public static function getDocuments($document, $item, $field = 'Document')
    {
        if (isset($item['Documents'][$document]) && is_array($item['Documents'][$document]) && !empty($item['Documents'][$document])) {
            return array_filter(
                array_map(function ($doc) use ($field) {
                    return (isset($doc['@attributes']['Class']) && $doc['@attributes']['Class'] === $field) ? $doc : null;
                }, $item['Documents'][$document]),
                function ($item) {
                    if ($item) {
                        return $item;
                    }
                }
            );
        } else {
            return $item['Documents'][$document] ?? [];
        }
    }

    public static function getAddresses($item, $address)
    {
        return $item['Addresses'][$address] ?? '';
    }

    public static function getAttribute($attribute, $item, $language, $details = true)
    {
        if ($details) {
            $item = $item['Details'] ?? $item;
        }
        if (isset($item[$attribute])) {
            foreach ($item[$attribute]['Translation'] as $value) {
                if (isset($value['@attributes'])) {
                    if (isset($value['@attributes']['Language']) && $value['@attributes']['Language'] == $language) {
                        return $value['@content'] ?? '';
                    }
                } else {
                    return $value;
                }
            }
        }
        return '';
    }

    public static function getTopics($item)
    {
        $topics = $item['Details']['Topics']['Topic'] ?? '';
        $ids = [];
        if ($topics) {
            if (is_array($topics) && !empty($topics)) {
                $type = $item['Details']['Topics']['@attributes']['Type'] ?? '';
                $ids[$type] = [];
                foreach ($topics as $topic) {
                    if (isset($topic['@attributes']) && $topic['@attributes']['Id']) {
                        array_push($ids[$type], $topic['@attributes']['Id']);
                    } else {
                        if (isset($topic['Id']) && $topic['Id']) {
                            array_push($ids[$type], $topic['Id']);
                        }
                    }
                }
            }
        }
        return $ids;
    }

    public static function getHolidayThemes($item)
    {
        $holiday_themes = $item['Details']['HolidayThemes']['Item'] ?? '';
        $ids = [];
        if ($holiday_themes) {
            if (is_array($holiday_themes) && !empty($holiday_themes)) {
                foreach ($holiday_themes as $theme) {
                    if (isset($theme['@attributes']) && $theme['@attributes']['Id']) {
                        array_push($ids, strtolower($theme['@attributes']['Id']));
                    } else {
                        if (isset($theme['Id']) && $theme['Id']) {
                            array_push($ids, strtolower($theme['Id']));
                        }
                    }
                }
            }
        }
        return $ids;
    }

    /* public static function isImage($document)
    {
        if (isset($document['@attributes']['Type']) && $document['@attributes']['Type'] === 'ServiceProvider') {
            if (isset($document['URL']) && (str_contains($document['URL'], '.jpg') || str_contains($document['URL'], '.jpeg') || str_contains($document['URL'], '.png') || str_contains($document['URL'], '.webp'))) {
                return $document['URL'];
            }
            return false;
        }
    }

    public static function isFile($document)
    {
        if (isset($document['@attributes']['Type']) && $document['@attributes']['Type'] === 'ServiceProvider') {
            if (isset($document['URL']) && (!str_contains($document['URL'], '.jpg') && !str_contains($document['URL'], '.jpeg') && !str_contains($document['URL'], '.png') && !str_contains($document['URL'], '.webp'))) {
                return $document['URL'];
            }
            return false;
        }
    }

    public static function searchForImages($document)
    {
        if (isset($document['URL'])) {
            return static::isImage($document);
        } else {
            foreach ($document as $doc) {
                return static::isImage($doc);
            }
        }
    }

    public static function searchForFiles($document)
    {
        if (isset($document['URL'])) {
            return static::isFile($document);
        } else {
            foreach ($document as $doc) {
                return static::isFile($doc);
            }
        }
    }

    public static function getGallery($documents)
    {
        if (!empty($documents)) {
            $gallery = array_map([__CLASS__, 'searchForImages'], $documents);
            return $gallery;
        }
    }

    public static function getAttachments($documents)
    {
        if (!empty($documents)) {
            $attachments = array_map([__CLASS__, 'searchForFiles'], $documents);
            return $attachments;
        }
    } */
}
