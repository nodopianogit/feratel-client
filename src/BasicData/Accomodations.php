<?php

namespace Nodopiano\Feratel\BasicData;

use Tightenco\Collect\Support\Collection;

class Accomodations extends BasicData
{
    public function get()
    {
        return Collection::make($this->results['Result']['ServiceProviders']['ServiceProvider']);
    }

    public static function getItemDetails($item, $language)
    {
        return [
            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => static::getDetail($item, 'Name'),
            'descriptions' => self::getDescriptions($item, $language),
            'city' => static::getDetail($item, 'Town'),
            'type' => static::getDetail($item, 'Type'),
            'thumbnail' => self::getThumbnail($item) ?? '',
            'position' => static::getDetail($item, 'Position') ? static::getDetail($item, 'Position')['@attributes'] : '',
            'addresses' => static::getAddresses($item, 'Address') ? static::getAddresses($item, 'Address') : '',
            'gallery' => self::getDocuments($item, 'Document') ?? null,
        ];
    }

    public static function getDescriptions($item, $language)
    {
        $description = [];

        if (isset($item['Descriptions'])) {
            foreach ($item['Descriptions']['Description'] as $value) {
                if (isset($value['@attributes']) && isset($value['@attributes']['Language']) && $value['@attributes']['Language'] == $language) {
                    $description[] = $value['@content'] ?? '';
                } else {
                    $description[] = $value['@content'] ?? isset($value['Id']) ? '' : $value;
                }
            }
        }

        return $description;
    }

    public static function getThumbnail($item)
    {
        if (isset($item['Documents'])) {
            if (!empty($item['Documents'])) {
                foreach ($item['Documents'] as $document) {
                    if (isset($document['URL'])) {
                        if (isset($document['@attributes']['Type']) && $document['@attributes']['Type'] === 'ServiceProvider') {
                            if (isset($document['URL']) && (str_contains($document['URL'], '.jpg') || str_contains($document['URL'], '.jpeg') || str_contains($document['URL'], '.png') || str_contains($document['URL'], '.webp'))) {
                                return $document['URL'];
                            }
                            return '';
                        }
                    } else {
                        foreach ($document as $doc) {
                            if (isset($doc['@attributes']['Type']) && $doc['@attributes']['Type'] === 'ServiceProvider') {
                                if (isset($doc['URL']) && (str_contains($doc['URL'], '.jpg') || str_contains($doc['URL'], '.jpeg') || str_contains($doc['URL'], '.png') || str_contains($doc['URL'], '.webp'))) {
                                    return $doc['URL'];
                                }
                                return '';
                            }
                        }
                    }
                }
            }
        }
    }

    /* public static function getGallery($item)
    {
        $gallery = [];
        if (isset($item['Documents']['Document']) && is_array($item['Documents']['Document']) && !empty($item['Documents']['Document'])) {
            $i = 0;
            foreach ($item['Documents']['Document'] as $slide) {
                $i++;
                if (isset($slide['URL'])) {
                    $gallery[] = ['url' => $slide['URL'], 'width' => 1440, 'height' => 900, 'alt' => 'Image - ' . $i, 'caption' => ''];
                }
            }
        }
        return $gallery;
    } */
}
