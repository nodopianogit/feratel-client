<?php

namespace Nodopiano\Feratel\BasicData;

use Tightenco\Collect\Support\Collection;

class AdditionalServices extends BasicData
{
    public function get()
    {
        $experiences_list = [];
        if (isset($this->results['Result']['ServiceProviders']['ServiceProvider']) && is_array($this->results['Result']['ServiceProviders']['ServiceProvider']) && !empty($this->results['Result']['ServiceProviders']['ServiceProvider'])) {
            foreach ($this->results['Result']['ServiceProviders']['ServiceProvider'] as $service_provider) {
                if (isset($service_provider['AdditionalServices']) && is_array($service_provider['AdditionalServices']['AdditionalService']) && !empty($service_provider['AdditionalServices'])) {
                    if (isset($service_provider['AdditionalServices']['AdditionalService']['Details'])) {
                        $id = $service_provider['AdditionalServices']['AdditionalService']['@attributes']['Id'] ?? '';
                        $experiences_list[$id] = $service_provider['AdditionalServices']['AdditionalService'];
                        $experiences_list[$id]['service_provider'] = $service_provider;
                    } else {
                        foreach ($service_provider['AdditionalServices']['AdditionalService'] as $experience) {
                            $id = $experience['@attributes']['Id'] ?? '';
                            $experiences_list[$id] = $experience;
                            $experiences_list[$id]['service_provider'] = $service_provider;
                        }
                    }
                }
            }
        }
        return Collection::make($experiences_list);
    }

    public static function getItemDetails($item, $language)
    {
        $gallery = self::getGallery($item, $language);
        return [
            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => static::getAttribute('Name', $item, $language),
            'dates' => static::getDetail($item, 'Dates') ?? '',
            'thumbnail' => self::getThumbnail($item) ?? '',
            'addresses' => static::getAddresses($item['service_provider'], 'Address') ?? '',
            'descriptions' => self::getDescriptions($item, $language),
            'contacts' => self::getAddresses($item['service_provider'], 'Address'),
            'holidayThemes' => static::getHolidayThemes($item['Products']['Product']) ?? null,
            'price' => self::getPrices($item['Products']),
            'buy_link' => self::getBuyLink($item),
            'gallery' => $gallery,
            'attachments' => self::getAttachments($item, $language),
        ];
    }

    public static function getThumbnail($item)
    {
        if (isset($item['Documents']['Document']['URL']) && (str_contains($item['Documents']['Document']['URL'], '.jpg') || str_contains($item['Documents']['Document']['URL'], '.jpeg') || str_contains($item['Documents']['Document']['URL'], '.png') || str_contains($item['Documents']['Document']['URL'], '.webp'))) {
            return $item['Documents']['Document']['URL'];
        } elseif (isset($item['Documents']['Document'][0]) && isset($item['Documents']['Document'][0]['URL']) && (str_contains($item['Documents']['Document'][0]['URL'], '.jpg') || str_contains($item['Documents']['Document'][0]['URL'], '.jpeg') || str_contains($item['Documents']['Document'][0]['URL'], '.png') || str_contains($item['Documents']['Document'][0]['URL'], '.webp'))) {
            return $item['Documents']['Document'][0]['URL'];
        }
    }

    public static function getDescriptions($item, $language)
    {
        $descriptions = [];

        if (isset($item['Descriptions'])) {
            foreach ($item['Descriptions'] as $description) {
                if (is_array($description) && !empty($description)) {
                    foreach ($description as $value) {
                        if (isset($value['@attributes']['Language'])) {
                            if ($value['@attributes']['Language'] === $language) {
                                $descriptions[] = $value['@content'] ?? '';
                            }
                        } else {
                            $descriptions[] = $value['@content'] ?? '';
                        }
                    }
                } else {
                    if (isset($value['@attributes']['Language'])) {
                        if ($value['@attributes']['Language'] === $language) {
                            $descriptions[] = $value['@content'] ?? '';
                        }
                    } else {
                        $descriptions[] = $value['@content'] ?? '';
                    }
                }
            }
        }

        return $descriptions;
    }

    public static function getPrices($item)
    {
        $prices = [];
        foreach ($item as $products) {
            foreach ($products as $product) {
                $product = $product['PriceDetail'] ?? $product;
                if (isset($product['PriceTemplates']['PriceTemplate']) && is_array($product['PriceTemplates']['PriceTemplate']) && !empty($product['PriceTemplates']['PriceTemplate'])) {
                    foreach ($product['PriceTemplates']['PriceTemplate'] as $price_template) {
                        $price_name = $price_template['@attributes']['Name'] ?? $price_template['Name'] ?? 'STANDARD';
                        if (isset($price_template['Prices'])) {
                            foreach ($price_template['Prices'] as $price) {
                                $price_value = $price['PriceValue'] ?? $price;
                                // Scrivo solo il prezzo STANDARD e se è diverso da 0
                                if ($price_name === 'STANDARD' && $price_value['@attributes']['Price']) {
                                    $prices['name'] = $price_name;
                                    $prices['value'] = $price_value['@attributes']['Price'] ?? 0;
                                    return array_filter($prices);
                                }
                            }
                        }
                    }
                }
            }
        }
        return array_filter($prices);
    }

    public static function getBuyLink($item)
    {
        $root = 'https://www.desklineitalia.it/res/tosc5/consorzioprealpi/index.html#/esperienze/';
        $db_id = static::getDetail($item['service_provider'], 'DBCode') ?? '';
        $experience_id = $item['@attributes']['Id'] ?? '';

        return "$root$db_id/$experience_id";
    }

    /* public static function getGallery($item, $language)
    {
        $gallery = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents'] as $image) {
                if (isset($image['Names']) && !empty($image['Names'])) {
                    foreach ($image['Names'] as $translations) {
                        if (is_array($translations) && !empty($translations)) {
                            foreach ($translations as $translation) {
                                if (isset($translation['@attributes']['Language']) && $translation['@attributes']['Language'] === $language) {
                                    $caption = (isset($translation['@content'])) ? $translation['@content'] : '';
                                }
                            }
                        }
                    }
                }
                if (isset($image['URL']) && (str_contains($image['URL'], '.jpg') || str_contains($image['URL'], '.jpeg') || str_contains($image['URL'], '.png') || str_contains($image['URL'], '.webp'))) {
                    $gallery[] = ['url' => $image['URL'], 'caption' => $caption];
                }
            }
        }
        return $gallery;
    } */

    public static function attachmentTitle($attachment, $language)
    {
        if (isset($attachment['Names']) && !empty($attachment['Names'])) {
            foreach ($attachment['Names'] as $translations) {
                if (is_array($translations) && !empty($translations)) {
                    foreach ($translations as $translation) {
                        if (isset($translation['@attributes']['Language']) && $translation['@attributes']['Language'] === $language) {
                            return $translation['@content'] ?? '';
                        }
                    }
                }
            }
        }
    }

    public static function isImage($image, $caption)
    {
        if (isset($image['URL']) && (str_contains($image['URL'], '.jpg') || str_contains($image['URL'], '.jpeg') || str_contains($image['URL'], '.png') || str_contains($image['URL'], '.webp'))) {
            return ['url' => $image['URL'], 'caption' => $caption];
        }
    }

    public static function isFile($file, $caption)
    {
        if (isset($file['URL']) && (!str_contains($file['URL'], '.jpg') && !str_contains($file['URL'], '.jpeg') && !str_contains($file['URL'], '.png') && !str_contains($file['URL'], '.webp'))) {
            return ['url' => $file['URL'], 'caption' => $caption];
        }
    }

    public static function getGallery($item, $language)
    {
        $gallery = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents']['Document'] as $image) {
                $caption = self::attachmentTitle($image, $language);
                if (self::isImage($image, $caption)) {
                    $gallery[] = self::isImage($image, $caption);
                }
            }
        }
        return $gallery;
    }

    public static function getAttachments($item, $language)
    {
        $attachments = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents']['Document'] as $file) {
                $caption = self::attachmentTitle($file, $language);
                if (self::isFile($file, $caption)) {
                    $attachments[] = self::isFile($file, $caption);
                }
            }
        }
        return $attachments;
    }
}
