<?php

namespace Nodopiano\Feratel\BasicData;

use Tightenco\Collect\Support\Collection;

class Brochures extends BasicData
{
    public function get()
    {
        return Collection::make($this->results['Result']['ShopItems']['ShopItem']);
    }

    public static function getItemDetails($item, $language)
    {
        return [
            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => static::getAttribute('Names', $item, $language),
            'thumbnails' => static::getDocuments('Document', $item, 'Image') ?? '',
            'documents' => static::getDocuments('Document', $item) ?? ''
        ];
    }
}
