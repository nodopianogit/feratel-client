<?php

namespace Nodopiano\Feratel\BasicData;

use Tightenco\Collect\Support\Collection;

class HotPoints extends BasicData
{
    public function get()
    {
        return Collection::make($this->results['Result']['Infrastructure']['InfrastructureItem']);
    }

    public static function getItemDetails($item, $language)
    {
        $gallery = self::getGallery($item, $language);
        $return = [

            'tosc_item_id' => $item['@attributes']['Id'] ?? '',
            'name' => static::getAttribute('Names', $item, $language),
            'descriptions' => self::getDescriptions($item, $language),
            'excerpt' => self::getExcerpt($item, $language),
            'addresses' => static::getAddresses($item, 'Address'),
            'position' => self::getDetail($item, 'Position')['@attributes'] ?? '',
            'thumbnail' => $gallery[0]['url'] ?? "",
            'gallery' => $gallery,
            'attachments' => self::getAttachments($item, $language),
            'topics' => static::getTopics($item) ?? null
        ];

        return $return;
    }

    public static function getExcerpt($item, $language)
    {
        $excerpt = '';

        if (isset($item['Descriptions'])) {
            foreach ($item['Descriptions']['Description'] as $value) {
                if (isset($value['@attributes']) && isset($value['@attributes']['Language']) && $value['@attributes']['Language'] == $language) {
                    if ($value['@attributes'] && isset($value['@attributes']['Type']) && $value['@attributes']['Type'] === 'InfrastructureShort') {
                        $excerpt = $value['@content'] ?? '';
                    }
                }
            }
        }
        return $excerpt;
    }

    public static function getDescriptions($item, $language)
    {
        $description = [];

        if (isset($item['Descriptions'])) {
            foreach ($item['Descriptions']['Description'] as $value) {
                if (isset($value['@attributes']) && isset($value['@attributes']['Language']) && $value['@attributes']['Language'] == $language) {
                    if ($value['@attributes'] && isset($value['@attributes']['Type']) && $value['@attributes']['Type'] === 'InfrastructureLong') {
                        $description['long'] = $value['@content'] ?? '';
                    } else if ($value['@attributes'] && isset($value['@attributes']['Name'])) {
                        if ($value['@attributes']['Name'] === 'Price Information') {
                            $description['prices'] = $value['@content'] ?? '';
                        }
                        if ($value['@attributes']['Name'] === 'Hint / Advice') {
                            $description['hints'] = $value['@content'] ?? '';
                        }
                    }
                } else {
                    $description[] = $value['@content'] ?? isset($value['Id']) ? '' : $value;
                }
            }
        }

        return $description;
    }

    public static function getThumbnail($item)
    {
        if (isset($item['Documents']['Document']['URL']) && (str_contains($item['Documents']['Document']['URL'], '.jpg') || str_contains($item['Documents']['Document']['URL'], '.jpeg') || str_contains($item['Documents']['Document']['URL'], '.png') || str_contains($item['Documents']['Document']['URL'], '.webp'))) {
            return $item['Documents']['Document']['URL'];
        } elseif (isset($item['Documents']['Document'][0]) && isset($item['Documents']['Document'][0]['URL']) && (str_contains($item['Documents']['Document'][0]['URL'], '.jpg') || str_contains($item['Documents']['Document'][0]['URL'], '.jpeg') || str_contains($item['Documents']['Document'][0]['URL'], '.png') || str_contains($item['Documents']['Document'][0]['URL'], '.webp'))) {
            return $item['Documents']['Document'][0]['URL'];
        }
    }

    public static function attachmentTitle($attachment, $language)
    {
        if (isset($attachment['Names']) && !empty($attachment['Names'])) {
            foreach ($attachment['Names'] as $translations) {
                if (is_array($translations) && !empty($translations)) {
                    foreach ($translations as $translation) {
                        if (isset($translation['@attributes']['Language']) && $translation['@attributes']['Language'] === $language) {
                            return $translation['@content'] ?? '';
                        }
                    }
                }
            }
        }
    }

    public static function isImage($image, $caption)
    {
        if (isset($image['URL']) && (str_contains($image['URL'], '.jpg') || str_contains($image['URL'], '.jpeg') || str_contains($image['URL'], '.png') || str_contains($image['URL'], '.webp'))) {
            return ['url' => $image['URL'], 'caption' => $caption];
        }
    }

    public static function isFile($file, $caption)
    {
        if (isset($file['URL']) && (!str_contains($file['URL'], '.jpg') && !str_contains($file['URL'], '.jpeg') && !str_contains($file['URL'], '.png') && !str_contains($file['URL'], '.webp'))) {
            return ['url' => $file['URL'], 'caption' => $caption];
        }
    }

    public static function scanDocuments($documents, $language)
    {
        $gallery = [];
        foreach ($documents as $image) {
            if (isset($image['URL'])) {
                $caption = self::attachmentTitle($image, $language);
                if (self::isImage($image, $caption)) {
                    $gallery[] = self::isImage($image, $caption);
                }
            } else {
                $gallery = array_merge($gallery, self::scanDocuments($image, $language));
            }
        }
        return $gallery;
    }

    public static function getGallery($item, $language)
    {
        $gallery = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            $gallery = array_merge($gallery, self::scanDocuments($item['Documents'], $language));
        }
        return $gallery;
    }

    public static function getAttachments($item, $language)
    {
        $attachments = [];
        if (isset($item['Documents']) && is_array($item['Documents'])) {
            foreach ($item['Documents']['Document'] as $file) {
                $caption = self::attachmentTitle($file, $language);
                if (self::isFile($file, $caption)) {
                    $attachments[] = self::isFile($file, $caption);
                }
            }
        }
        return $attachments;
    }
}
