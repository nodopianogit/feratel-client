<?php

use Nodopiano\Feratel\BasicData\Events;
use Nodopiano\Feratel\BasicData\Packages;
use Nodopiano\Feratel\BasicData\Accomodations;
use Nodopiano\Feratel\FeratelClient;
use PHPUnit\Framework\TestCase;
use Tightenco\Collect\Support\Collection;

class FeratelTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->client = new FeratelClient('N0D0P1AN0', 'F180FFD5-4FBF-4F2C-AC00-7E8B94462F2C');
    }

    public function testFeratelClient()
    {
        $this->assertInstanceOf(FeratelClient::class, $this->client);
    }

    public function test_get_accomodations_returns_a_collection()
    {
        $accomodations = (new Accomodations($this->client))->query(__DIR__ . '/serviceprovider.xml')->get();
        $this->assertInstanceOf(Collection::class, $accomodations);
    }

    public function test_get_events_returns_a_collection()
    {
        $events = (new Events($this->client))->query(__DIR__ . '/events.xml')->get();
        $this->assertInstanceOf(Collection::class, $events);
    }

    public function test_get_packages_returns_a_collection()
    {
        $packages = (new Packages($this->client))->query(__DIR__ . '/packages.xml')->get();
        $this->assertInstanceOf(Collection::class, $packages);
    }
}
