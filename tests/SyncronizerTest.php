<?php

namespace Nodopiano\Feratel\Importer\Sync;

use PHPUnit\Framework\TestCase;
use Nodopiano\Feratel\FeratelClient;
use Nodopiano\Feratel\BasicData\Events;
use Nodopiano\Feratel\BasicData\Packages;
use Nodopiano\Feratel\Importer\Sync\EventsSynchronizer;
use Nodopiano\Feratel\Importer\Sync\ExperiencesSynchronizer;
use Nodopiano\Feratel\Importer\Sync\AccomodationsSynchronizer;

class SyncronizerTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->client = new FeratelClient('N0D0P1AN0', 'F180FFD5-4FBF-4F2C-AC00-7E8B94462F2C');
    }

    public function test_events_gets_formatted_by_language()
    {
        $events = (new Events($this->client))->query(__DIR__ . '/events.xml')->get();
        $eventsArray = Events::format($events);
        $this->assertArrayHasKey('it', $eventsArray->first());
    }

    public function test_events_gets_imported_into_wordpress()
    {
        $this->assertTrue(EventsSynchronizer::sync($this->client));
    }

    public function text_experiences_gets_formatted_by_language()
    {
        $events = (new Packages($this->client))->query(__DIR__ . '/packages.xml')->get();
        $eventsArray = Packages::format($events);
        $this->assertArrayHasKey('it', $eventsArray->first());
    }

    public function test_experiences_gets_imported_into_wordpress()
    {
        $this->assertTrue(ExperiencesSynchronizer::sync($this->client));
    }

    public function test_accomodations_gets_imported_into_wordpress()
    {
        $this->assertTrue(AccomodationsSynchronizer::sync($this->client));
    }
}

if (!function_exists('post_exists')) {
    function post_exists()
    {
        return false;
    }
}

if (!function_exists('wp_insert_post')) {
    function wp_insert_post(array $post)
    {
        return random_int(1, 200);
    }
}

if (!function_exists('update_field')) {
    function update_field($field_name, $value, $post_id)
    {
        return true;
    }
}

if (!function_exists('apply_filters')) {
    function apply_filters($filter, $empty_value = null, $post_id = null)
    {
        $filters = [];
        $filters['wpml_element_type'] = 'events';
        $filters['wpml_element_language_details'] = (object) [
            'language_code' => 'it',
            'locale' => '',
            'text_direction' => '',
            'display_name' =>  '',
            'native_name' => '',
            'trid' => 12,
            'different_language' => ''
        ];
        return $filters[$filter];
    }
}
if (!function_exists('do_action')) {
    function do_action($name, $function)
    {
        return true;
    }
}
