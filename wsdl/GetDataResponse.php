<?php

class GetDataResponse
{

    /**
     * @var string $GetDataResult
     * @access public
     */
    public $GetDataResult = null;

    /**
     * @param string $GetDataResult
     * @access public
     */
    public function __construct($GetDataResult)
    {
      $this->GetDataResult = $GetDataResult;
    }

}
