<?php

class GetAvailabilityResponse
{

    /**
     * @var string $GetAvailabilityResult
     * @access public
     */
    public $GetAvailabilityResult = null;

    /**
     * @param string $GetAvailabilityResult
     * @access public
     */
    public function __construct($GetAvailabilityResult)
    {
      $this->GetAvailabilityResult = $GetAvailabilityResult;
    }

}
