<?php

include_once('GetData.php');
include_once('GetDataResponse.php');
include_once('GetAvailability.php');
include_once('GetAvailabilityResponse.php');
include_once('SetClientIP.php');
include_once('SetClientIPResponse.php');

class BasicData extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetData' => '\GetData',
      'GetDataResponse' => '\GetDataResponse',
      'GetAvailability' => '\GetAvailability',
      'GetAvailabilityResponse' => '\GetAvailabilityResponse',
      'SetClientIP' => '\SetClientIP',
      'SetClientIPResponse' => '\SetClientIPResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'input.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetData $parameters
     * @access public
     * @return GetDataResponse
     */
    public function GetData(GetData $parameters)
    {
      return $this->__soapCall('GetData', array($parameters));
    }

    /**
     * @param GetAvailability $parameters
     * @access public
     * @return GetAvailabilityResponse
     */
    public function GetAvailability(GetAvailability $parameters)
    {
      return $this->__soapCall('GetAvailability', array($parameters));
    }

    /**
     * @param SetClientIP $parameters
     * @access public
     * @return SetClientIPResponse
     */
    public function SetClientIP(SetClientIP $parameters)
    {
      return $this->__soapCall('SetClientIP', array($parameters));
    }

}
