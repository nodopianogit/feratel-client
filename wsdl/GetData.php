<?php

class GetData
{

    /**
     * @var string $xmlString
     * @access public
     */
    public $xmlString = null;

    /**
     * @param string $xmlString
     * @access public
     */
    public function __construct($xmlString)
    {
      $this->xmlString = $xmlString;
    }

}
