<?php

class SetClientIP
{

    /**
     * @var string $ip
     * @access public
     */
    public $ip = null;

    /**
     * @param string $ip
     * @access public
     */
    public function __construct($ip)
    {
      $this->ip = $ip;
    }

}
